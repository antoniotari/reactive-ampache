/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.managers;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.Gson;

import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampache.utils.FileUtil;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.models.LocalPlaylist;
import com.antoniotari.reactiveampacheapp.models.SavedPlaylists;

import rx.Observable;
import rx.Observable.OnSubscribe;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antonio tari on 2016-06-22.
 */
public enum PlaylistManager {
    INSTANCE;

    public interface OnPlaylistChangeListener {
        void onNewPlaylist(String name);
        void onPlaylistChanged(String name);
        void onPlaylistRemoved(String name);
    }

    private final TreeMap<String,List<Song>> playlists = new TreeMap<String,List<Song>>();
    private final List<OnPlaylistChangeListener> onPlaylistChangeListeners = new ArrayList<>();
    private Context applicationContext;

    public void init(Context context) {
        applicationContext = context;

        Observable.create(new OnSubscribe<SavedPlaylists>() {
            @Override
            public void call(final Subscriber<? super SavedPlaylists> subscriber) {
                try {
                    SavedPlaylists playlistWrap = new Gson()
                            .fromJson(FileUtil.getInstance().readStringFile(applicationContext, "savedLists"), SavedPlaylists.class);
                    if (playlistWrap == null) throw new NullPointerException("playlist on disc is null");
                    subscriber.onNext(playlistWrap);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(savedPlaylists -> playlists.putAll(savedPlaylists.playlists),
                        Log::error);
    }

    public void addOnPlaylistChangeListener(OnPlaylistChangeListener onPlaylistChangeListener) {
        onPlaylistChangeListeners.add(onPlaylistChangeListener);
    }

    public void removeOnPlaylistChangeListener(OnPlaylistChangeListener onPlaylistChangeListener) {
        onPlaylistChangeListeners.remove(onPlaylistChangeListener);
    }

    public void fireOnNewPlaylistListeners(String name) {
        for(OnPlaylistChangeListener onPlaylistChangeListener:onPlaylistChangeListeners) {
            if(onPlaylistChangeListener!=null) {
                onPlaylistChangeListener.onNewPlaylist(name);
            }
        }
    }

    public void fireOnPlaylistChangedListeners(String name) {
        for(OnPlaylistChangeListener onPlaylistChangeListener:onPlaylistChangeListeners) {
            if(onPlaylistChangeListener!=null) {
                onPlaylistChangeListener.onPlaylistChanged(name);
            }
        }
    }

    public void fireOnRemovePlaylistListeners(String name) {
        for(OnPlaylistChangeListener onPlaylistChangeListener:onPlaylistChangeListeners) {
            if(onPlaylistChangeListener!=null) {
                onPlaylistChangeListener.onPlaylistRemoved(name);
            }
        }
    }

    public ArrayList<String> getPlaylistNames(){
        return new ArrayList<>(playlists.keySet());
    }

    public List<Song> getPlaylist(String name){
        return playlists.get(name);
    }

    public boolean newPlaylist(String name) {
        if(getPlaylistNames().contains(name))return false;
        playlists.put(name,new ArrayList<>());
        // fire listeners
        fireOnNewPlaylistListeners(name);
        return getPlaylistNames().contains(name);
    }

    public boolean removePlaylist(final String name){
        if(!getPlaylistNames().contains(name)) return false;
        playlists.remove(name);
        fireOnRemovePlaylistListeners(name);
        savePlaylistsToDisc();
        return !getPlaylistNames().contains(name);
    }

    public void addToPlaylist(String playlistName, Song song){
        List<Song> songs = getPlaylist(playlistName);
        if (songs==null){
            songs = new ArrayList<>();
        }
        songs.add(song);
        playlists.put(playlistName, songs);
        fireOnPlaylistChangedListeners(playlistName);

        savePlaylistsToDisc();
    }

    public void addPlaylist(String playlistName, List<Song> songsList){
        if (songsList==null) return;

        playlists.put(playlistName, songsList);
        fireOnPlaylistChangedListeners(playlistName);

        savePlaylistsToDisc();
    }

    public void removeFromPlaylist(String playlistName, int position) {
        List<Song> songs = getPlaylist(playlistName);
        if (songs!=null && position>=0 && position < songs.size()) {
            songs.remove(position);
            playlists.put(playlistName, songs);
            Log.blu(playlistName, songs.size());
        }
        fireOnPlaylistChangedListeners(playlistName);
        savePlaylistsToDisc();
    }

    public List<LocalPlaylist> generateLocalPlaylists() {
        List<LocalPlaylist> localPlaylistList= new ArrayList<>();
        for (Map.Entry<String,List<Song>> entry : playlists.entrySet()) {
            localPlaylistList.add(new LocalPlaylist(entry.getValue(), entry.getKey()));
        }
        return localPlaylistList;
    }

    private void savePlaylistsToDisc() {
        Observable.create(new OnSubscribe<SavedPlaylists>() {
            @Override
            public void call(final Subscriber<? super SavedPlaylists> subscriber) {
                try {
                    SavedPlaylists playlistWrap = new SavedPlaylists();
                    playlistWrap.playlists = playlists;
                    FileUtil.getInstance().writeStringFile(applicationContext,"savedLists",new Gson().toJson(playlistWrap));
                    subscriber.onNext(playlistWrap);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> Log.log("file saved"), Log::error);
    }

    public String getOfflinePlaylistTitle() {
        return applicationContext.getString(R.string.offline_playlist);
    }

    public static boolean isOfflinePlaylist(@NonNull Playlist playlist) {
        return (playlist instanceof LocalPlaylist) && playlist.getName().equals(PlaylistManager.INSTANCE.getOfflinePlaylistTitle());
    }

    public LocalPlaylist getOfflinePlaylist(List<Song> offlineSongs) {
        return new LocalPlaylist(offlineSongs, PlaylistManager.INSTANCE.getOfflinePlaylistTitle());
//        return new LocalPlaylist(AmpacheCache.INSTANCE.getCachedList(applicationContext),
//                PlaylistManager.INSTANCE.getOfflinePlaylistTitle());
    }
}
