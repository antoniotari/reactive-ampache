package com.antoniotari.reactiveampacheapp.listeners;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.models.Album;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampacheapp.PlaySongBehaviour;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.utils.Utils;

import java.util.List;

/**
 * Created by antonio on 31/10/17.
 */
public class AlbumPlaySelectedListener implements View.OnClickListener {

    private Album album;
    private PlaySongBehaviour playSongBehaviour;
    private List<Song> songList;

    public AlbumPlaySelectedListener(Album album) {
        this.album = album;
    }

    @Override
    public void onClick(final View v) {
        Utils.getWaitToast(v.getContext()).show();
        AmpacheApi.INSTANCE.getSongsFromAlbum(album.getId())
                .subscribe(this::setSongList,throwable -> {},() -> onSongs(v.getContext()));
    }

    public void setSongList(List<Song> songList) {
        this.songList = songList;
    }

    private void onSongs(Context context) {
        if (context == null) return;
        if (songList == null || songList.isEmpty()) {
            Utils.onError(context, new Exception(context.getString(R.string.no_song_list)));
            return;
        }

        //Toast waitToast = Utils.getWaitToast(context);
        //waitToast.show();
        playSongBehaviour = new PlaySongBehaviour(songList.get(0), songList);
        new Handler(Looper.getMainLooper()).postDelayed( () -> playSongBehaviour.playSong(context), 400);
    }
}
