/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.managers.PlaylistManager;
import com.antoniotari.reactiveampacheapp.models.LocalPlaylist;
import com.antoniotari.reactiveampacheapp.utils.AmpacheCache;
import com.antoniotari.reactiveampacheapp.utils.Utils;

import static com.antoniotari.reactiveampacheapp.utils.Utils.setHtmlString;

/**
 * Created by antoniotari on 2016-06-21.
 */
public class PlaylistsAdapter extends SectionIndexerAdapter<Playlist, SongViewHolder> {

    public interface OnPlaylistClickListener {
        void onPlaylistClick(final Playlist playlist);
    }

    private List<Playlist> mPlaylists;
    private OnPlaylistClickListener mOnPlaylistClickListener;

    public PlaylistsAdapter(List<Playlist> playlists) {
        mPlaylists = playlists;
    }

    public void setPlaylists(List<Playlist> playlists) {
        mPlaylists = playlists;
    }

    public void setOnPlaylistClickListener(final OnPlaylistClickListener onPlaylistClickListener) {
        mOnPlaylistClickListener = onPlaylistClickListener;
    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_song, viewGroup, false);
        return new SongViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SongViewHolder viewHolder, int i) {
        final Playlist playlist = mPlaylists.get(i);
        String title = playlist.getName();
        setHtmlString(viewHolder.songName, title);
        Context context = viewHolder.songName.getContext();
        String songStr = playlist.getItems() == 1 ? context.getString(R.string.song) : context.getString(R.string.songs);
        viewHolder.songDuration.setText(String.format(songStr, playlist.getItems()));

        viewHolder.mainCardView.setOnClickListener(view -> {
            if (mOnPlaylistClickListener != null) {
                mOnPlaylistClickListener.onPlaylistClick(playlist);
            }
        });

        // do not show the menu
        viewHolder.songMenuButton.setVisibility(View.GONE);
        viewHolder.mainCardView.setCardBackgroundColor(context.getResources().getColor(R.color.card_bg));

        if (PlaylistManager.isOfflinePlaylist(playlist)) {
            viewHolder.mainCardView.setCardBackgroundColor(context.getResources().getColor(R.color.card_bg_offline));

            viewHolder.removePlaylist.setVisibility(View.VISIBLE);
            // TODO PlaylistManager.INSTANCE.fireOnNewPlaylistListeners(title) is manually firing the listeners, find a better way to update the fragment
            viewHolder.removePlaylist.setOnClickListener(v ->
                    Utils.showDialog(context, R.string.dialog_delete_cache_title, R.string.dialog_delete_cache_message,
                            () -> AmpacheCache.INSTANCE.clearCache(context).subscribe(bool -> PlaylistManager.INSTANCE.fireOnNewPlaylistListeners(title))));
        } else if (playlist.getName().equals(context.getString(R.string.current_playlist))) {
            viewHolder.mainCardView.setCardBackgroundColor(context.getResources().getColor(R.color.card_bg_current));

        } else if(playlist instanceof LocalPlaylist) {
            viewHolder.removePlaylist.setVisibility(View.VISIBLE);
            viewHolder.removePlaylist.setOnClickListener(v ->
                    Utils.showDialog(context, R.string.dialog_delete_title, R.string.dialog_delete_message, () -> PlaylistManager.INSTANCE.removePlaylist(title)));
        } else {
            viewHolder.removePlaylist.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (mPlaylists == null) {
            return 0;
        }
        return mPlaylists.size();
    }

    @Override
    protected List<Playlist> getItems() {
        return mPlaylists;
    }
}
